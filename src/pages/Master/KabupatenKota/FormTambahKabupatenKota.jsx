import { Form, Input } from 'antd';

const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

export default function FormTambahKabupatenKota({form}) {
    return (
        <Form {...layout} 
        form={form} 
        name="control-hooks"
        >
            <Form.Item name="nama_kabupaten" label="Nama Kabupaten / Kota" key="kab_kota">
                <Input type="text" key="kab_kota1"/>
            </Form.Item>
        </Form>
    )
}
