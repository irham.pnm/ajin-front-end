import React, { useEffect, useState } from "react";
import _MainLayouts from "../../../layouts/_MainLayouts";
import {
  _Button,
  _Checkbox,
  _Date,
  _Input,
  _Label,
  _Select,
  _Switch,
  _TitleBar,
} from "../../../services/Forms/Forms";
import {
  Table,
  Form,
  Pagination,
  Row,
  Col,
  Button,
  Popconfirm,
  message,
  Input
} from "antd";
import { PlusOutlined, DeleteFilled, EditFilled, SaveFilled } from "@ant-design/icons";
import { _Col, _Row } from "../../../services/Forms/LayoutBootstrap";
import _Api from "../../../services/Api/_Api";
import { _Toastr } from "../../../services/Toastr/Notify/_Toastr";
import _AutocompleteRev from "../../../services/Forms/_AutocompleteRev";
import ModalTambahPasar from "./ModalTambahPasar";

function Pasar() {

  const [form] = Form.useForm();
  const [input, setinput] = useState(false)
  const [paginate, setpaginate] = useState({
      total : 10,
      from : 1
  })
  const [page, setPage] = useState({
    size : 10,
    cp : 1,
})
  const [dataSurvey, setdataSurvey] = useState([])
  const [listData, setListData] = useState({
    petugas: [],
    pasar: []
  })
  const [dataKabupaten, setDataKabupaten] = useState([])
  const [selected, setselected] = useState("")
  const [loadingDel, setloadingDel] = useState(false)
  const [visible, setVisible] = useState(false);

  const [editingKey, setEditingKey] = useState('');
  const isEditing = (record) => record.id === editingKey;

  const [formData] = Form.useForm()
  const showTambahModal = ()=>{
    setVisible(true)
  }

  const onCancel = () => {
    setVisible(false) 
  }

  const edit = (record) => {
    form.setFieldsValue({
      nama_pasar: '',
      ...record,
    });
    setEditingKey(record.id);
  };

  const cancelEdit = () => {
    setEditingKey('');
  };

  const saveEdit = async (record) => {
    try {
      const row = await form.validateFields();
      const dataSave = {...row, petugas_pasar_id: record.petugas_pasar.id , desa_id:record.desa.id }
      console.log(dataSave)
      putEdit(record.id, dataSave)
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const EditableCell = ({
    editing,
    dataIndex,
    title,
    record,
    index,
    children,
    ...restProps
  }) => {
    return (
      <td {...restProps}>
        {editing ? (
          <Form
          form={form} 
          >
            <Form.Item
              name="nama_pasar"
              style={{
                margin: 0,
              }}
              rules={[
                {
                  required: true,
                  message: `Please Input ${title}!`,
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Form>
        ) : (
          children
        )}
      </td>
    );
  };

  const putEdit = (id, values) => {
    _Api.put(`pasar/${id}`,values)
    .then(() => {
        message.success('Update Berhasil', 3)
        loadData(page.cp, page.size)
        setEditingKey('');
    })
    .catch(info => {
        message.error('Update Gagal', 3)
    })
  }




  const columns = [
    {
      title: "No",
      width: 50,
      fixed: 'left',
      align: "center",
      render: (text, row, index) => <> {paginate.from + index } </>,
    },
    {
        title: "Nama Pasar",
        width: 250,
        fixed: 'left',
        editable: true,
        dataIndex: 'nama_pasar',
        sorter: (a, b) => a.nama_pasar.length - b.nama_pasar.length,
        render: (_, rc) => (
          <div> {rc.nama_pasar}</div>
        ),
    },
    {
      title: "Petugas",
      width: 250,
      sorter: (a, b) => a.petugas_pasar.nama.length - b.petugas_pasar.nama.length,
      render: (_, rc) => (
        <div> {rc.petugas_pasar.nama}</div>
      ),
    },
    {
        title: "Nama Desa",
        width: 250,
        sorter: (a, b) => a.desa.nama_desa.length - b.desa.nama_desa.length,
        render: (_, rc) => (
          <div> {rc.desa.nama_desa}</div>
        ),
    },
    {
      title: "Nama Kecamatan",
      width: 250,
      sorter: (a, b) => a.desa.kecamatan.nama_kecamatan.length - b.desa.kecamatan.nama_kecamatan.length,
      render: (_, rc) => (
        <div> {rc.desa.kecamatan.nama_kecamatan}</div>
      ),
    },
    {
        title: "Nama Kabupaten",
        width: 250,
        sorter: (a, b) => a.desa.kecamatan.kabupaten.nama_kabupaten.length - b.desa.kecamatan.kabupaten.nama_kabupaten.length,
        render: (_, rc) => (
          // <div> {moment(rc.tglregistrasi).format("DD-MM-YYYY HH:mm")}</div>
          <div> {rc.desa.kecamatan.kabupaten.nama_kabupaten}</div>
        ),
    },
    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 150,
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <>
            <Popconfirm title="Yakin Untuk menyimpan Data?" onConfirm={()=> saveEdit(record.id)}>
              <Button style={{marginRight: "5px"}} type="primary" ><SaveFilled /></Button>
            </Popconfirm>
            <Button style={{marginRight: "5px"}}  danger type="primary" onClick={cancelEdit}>Batal</Button>
          </>
        ): (
          <>
            <Popconfirm title="Yakin Ingin Menghapus Data?" onConfirm={() => deleteData()} key="popup1">
              <Button disabled={editingKey !== ''}  type="primary" style={{marginRight: "5px"}} danger><DeleteFilled /></Button>
            </Popconfirm>
            <Button disabled={editingKey !== ''} type="primary" onClick={() => edit(record)}><EditFilled /></Button>
          </>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });


  const deleteData = () => {
    if(selected.length === 0){
        message.warning('Data Belum Dipilih');
        return
    }
    _Api.delete(`pasar/${selected.id}`)
    .then((res)=>{
        message.success('Hapus Berhasil');
        loadData(page.cp, page.size)
    })
    .catch((info) => {
        message.error('Hapus gagal');
    })
  }

  const loadData = (cp, size) => {
    setloadingDel(true)
    _Api.get(`pasar?with[]=petugas_pasar&with[]=desa.kecamatan.kabupaten_kota&paginate=true&page=${cp}&per_page=${size}`).then(res => {
      setloadingDel(false)
      console.log('res.data :>> ', res.data);
      setdataSurvey(res.data.data)
      setpaginate(res.data.meta)
      // setMataKuliah(res.data)
    })
  }

  const onChangePageSize = (cp, size) => {
      console.log(cp,size)
      setPage({size : size, cp : cp})
      loadData(cp, size)
  }

  const loadCombo = () => {
    setloadingDel(true)
    _Api.get("w/desa").then(res => {
      const desaCombo = res.data.data
      _Api.get("manajemen-pengguna").then(res => {
        setListData({desa: desaCombo, petugas:res.data.data})
        setloadingDel(false)
      })
    })

  }


  useEffect(() => {
    loadData(1,10)
    loadCombo()
  }, [])

  return (
    <_MainLayouts>

      <_TitleBar label=" DATA MASTER PASAR" />
      <p style={{ marginBlock: "10px" }}></p>
      <ModalTambahPasar
          visible={visible}
          onCancel={() => onCancel()}
          getData={loadData}
          listData={listData}
      />
      <Row style={{padding: "5px"}}>
          <Col span={24}>
            <Button type="primary" style={{marginRight: '5px'}} onClick={()=> showTambahModal()} icon={<PlusOutlined/>}>Tambah</Button>
          </Col>
      </Row>
      <Table
        rowKey="id"
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        pagination={{ position: [], pageSize: page.size }} loading={loadingDel}
        columns={mergedColumns} dataSource={dataSurvey}
        scroll={{ x: 1, y: 1000 }}
        rowClassName={(record, index) => record == selected && 'bg-selected'}
        onRow={(record, rowIndex) => {
          return {
            onClick: event => {
              setselected(record)
            },
          };
        }}

      />
      <br />
      <Pagination style={{ width: "100%", textAlign: "center" }} 
                defaultCurrent={1} 
                total={parseInt(paginate.total)} 
                onChange={(cp, size) => onChangePageSize(cp, size)} 
                />

    </_MainLayouts>
  );
}

export default Pasar;
