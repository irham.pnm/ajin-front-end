import { Form, Input } from 'antd';
import { _Select } from '../../../services/Forms/Forms';

const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

export default function FormTambahPasar({form, listData}) {
    return (
        <Form {...layout} 
        form={form} 
        name="control-hooks"
        >
            <Form.Item name="nama_pasar" label="Nama Pasar" key="nama_pasar" rules={[{required: true}]}>
                <Input type="text" key="nama_pasar"/>
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}} name="petugas_pasar_id" label="Petugas Pasar" key="petugas_pasar_id" rules={[{required: true}]}>
                <_Select option={listData.petugas} val="id" caption="nama" />
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}} name="desa_id" label="Desa/Kelurahan" key="desa_id" rules={[{required: true}]}>
                <_Select option={listData.desa} val="id" caption="nama_desa" />
            </Form.Item>
        </Form>
    )
}
