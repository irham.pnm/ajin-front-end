import React, { useEffect, useState } from "react";
import _MainLayouts from "../../../layouts/_MainLayouts";
import {
  _Button,
  _Checkbox,
  _Date,
  _Input,
  _Label,
  _Select,
  _Switch,
  _TitleBar,
} from "../../../services/Forms/Forms";
import {
  Table,
  Form,
  Row,
  Col,
  Button,
  Pagination,
  message,
  Popconfirm,
  Input
} from "antd";
import { PlusOutlined, DeleteFilled, EditFilled, SaveFilled } from "@ant-design/icons";
import { _Col, _Row } from "../../../services/Forms/LayoutBootstrap";
import _Api from "../../../services/Api/_Api";
import { _Toastr } from "../../../services/Toastr/Notify/_Toastr";
import ModalTambahSatuan from "./ModalTambahSatuan";

function Satuan() {

  const [form] = Form.useForm();
  const [input, setinput] = useState(false)
  const [paginate, setpaginate] = useState({
    total : 10,
    from : 1
  })
  const [page, setPage] = useState({
    size : 10,
    cp : 1,
  })
  const [dataSurvey, setdataSurvey] = useState([])
  const [selected, setselected] = useState("")
  const [loadingDel, setloadingDel] = useState(false)
  const [visible, setVisible] = useState(false);

  const [editingKey, setEditingKey] = useState('');

  const isEditing = (record) => record.id === editingKey;

  const edit = (record) => {
    form.setFieldsValue({
      nama_satuan: '',
      ...record,
    });
    setEditingKey(record.id);
  };

  const cancelEdit = () => {
    setEditingKey('');
  };

  const EditableCell = ({
    editing,
    dataIndex,
    title,
    record,
    index,
    children,
    ...restProps
  }) => {
    return (
      <td {...restProps}>
        {editing ? (
          <Form
          form={form} 
          >
            <Form.Item
              name="nama_satuan"
              style={{
                margin: 0,
              }}
              rules={[
                {
                  required: true,
                  message: `Please Input ${title}!`,
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Form>
        ) : (
          children
        )}
      </td>
    );
  };

  

  const showTambahModal = ()=>{
    setVisible(true)
  }

  const onCancel = () => {
    setVisible(false) 
  }

  const columns = [
    {
      title: "No",
      width: 50,
      align: "center",
      render: (text, row, index) => <> {paginate.from+index} </>,
    },
    {
      title: "Satuan",
      width: 300,
      editable: true,
      dataIndex: 'nama_satuan',
      sorter: (a, b) => a.nama_satuan.length - b.nama_satuan.length,
      render: (_, rc) => (
        <div> {rc.nama_satuan}</div>
      ),
    },
    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 100,
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <>
            <Popconfirm title="Yakin Untuk menyimpan Data?" onConfirm={()=> saveEdit(record.id)}>
              <Button style={{marginRight: "5px"}} ><SaveFilled /></Button>
            </Popconfirm>
            <Button style={{marginRight: "5px"}} onClick={cancelEdit}>Cancel</Button>
          </>
        ): (
          <>
            <Popconfirm title="Yakin Ingin Menghapus Data?" onConfirm={() => deleteData()} key="popup1">
              <Button disabled={editingKey !== ''} style={{marginRight: "5px"}} danger><DeleteFilled /></Button>
            </Popconfirm>
            <Button disabled={editingKey !== ''} onClick={() => edit(record)}><EditFilled /></Button>
          </>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  const saveEdit = async (key) => {
    try {
      const row = await form.validateFields();
      console.log(row)
      putEdit(key, row)
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const putEdit = (id, values) => {
    _Api.put(`satuan/${id}`,values)
    .then(() => {
        message.success('Update Berhasil', 3)
        loadData(page.cp, page.size)
        setEditingKey('');
    })
    .catch(info => {
        message.error('Update Gagal', 3)
    })
  }



  const loadData = (cp, size) => {
    setloadingDel(true)
    _Api.get(`satuan?paginate=true&page=${cp}&per_page=${size}`).then(res => {
      setloadingDel(false)
      console.log('res.data :>> ', res.data);
      setdataSurvey(res.data.data)
      setpaginate(res.data.meta)
      // setMataKuliah(res.data)
    })
  }

  const deleteData = () => {
    if(selected.length === 0){
        message.warning('Data Belum Dipilih');
        return
    }
    _Api.delete(`satuan/${selected.id}`)
    .then((res)=>{
        message.success('Hapus Berhasil');
        loadData(page.cp, page.size)
    })
    .catch((info) => {
        message.error('Hapus gagal');
    })
}

  const onChangePageSize = (cp, size) => {
    setPage({size : size, cp : cp})
    loadData(cp, size)
  }


  useEffect(() => {
    loadData(1,10)
  }, [])

  return (
    <_MainLayouts>

      <_TitleBar label=" DATA MASTER SATUAN" />
      <p style={{ marginBlock: "10px" }}></p>
      <Row style={{padding: "5px"}}>
          <Col span={24}>
            <Button type="primary" style={{marginRight: '5px'}} onClick={()=> showTambahModal()} icon={<PlusOutlined/>}>Tambah</Button>
          </Col>
      </Row>
      <ModalTambahSatuan
          visible={visible}
          onCancel={() => onCancel()}
          getData={loadData}
      />
      <Table
        rowKey="id"
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        pagination={{ position: [], pageSize: 10 }} loading={loadingDel}
        columns={mergedColumns} dataSource={dataSurvey}
        scroll={{ x: 1, y: 1000 }}
        rowClassName={(record, index) => record == selected && 'bg-selected'}
        onRow={(record, rowIndex) => {
          return {
            onClick: event => {
              setselected(record)
            },
          };
        }}

      />
      <br />
      <Pagination style={{ width: "100%", textAlign: "center" }} 
                defaultCurrent={1} 
                total={parseInt(paginate.total)}
                onChange={(cp, size) => onChangePageSize(cp, size)} 
                />
      

    </_MainLayouts>
  );
}

export default Satuan;
