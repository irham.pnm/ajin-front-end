import { Modal, Form, message } from 'antd';
import _Api from '../../../services/Api/_Api';
import FormTambahSatuan from './FormTambahSatuan';


const ModalTambahSatuan = ({ visible, onCancel, getData }) => {
    const [form] = Form.useForm();

    const handleOk = () => {
        form
          .validateFields()
          .then((values) => {
            let dataSave = values
            console.log(dataSave)
            postData(dataSave)
          })
          .catch(() => {
                message.error('Lengkapi Data', 3)
          });
    };

    const postData = (values) => {
        _Api.post('satuan/',values)
        .then(() => {
            message.success('Simpan Berhasil', 3)
            getData(1,10)
            handleCancel()
        })
        .catch(info => {
            message.error('Simpan Gagal', 3)
        })
    }

    const handleCancel = () => {
        form.resetFields();
        onCancel()
    }

    

    let formIsian;
    formIsian = <FormTambahSatuan form={form}/>;

    return (
      <Modal
        visible={visible}
        title="Tambah Kabupaten/Kota"
        okText="Simpan"
        cancelText="Batal"
        onCancel={handleCancel}
        onOk={handleOk}
        closable={false}
        destroyOnClose={true}
      >
        {formIsian}
      </Modal>
    );
  };

  export default ModalTambahSatuan;