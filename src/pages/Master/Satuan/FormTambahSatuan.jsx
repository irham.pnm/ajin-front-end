import { Form, Input } from 'antd';

const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

export default function FormTambahSatuan({form}) {
    return (
        <Form {...layout} 
        form={form} 
        name="control-hooks"
        >
            <Form.Item name="nama_satuan" label="Satuan" key="nama_satuan">
                <Input type="text" key="nama_satuan"/>
            </Form.Item>
        </Form>
    )
}
