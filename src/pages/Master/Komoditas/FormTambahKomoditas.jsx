import { Form, Input, InputNumber } from 'antd';
import { _Select } from '../../../services/Forms/Forms';

const {TextArea} = Input
const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

export default function FormTambahKomoditas({form, listData}) {
    return (
        <Form {...layout} 
        form={form} 
        name="control-hooks"
        initialValues={{kode_komoditi: null, deskripsi: null}}
        >
            <Form.Item name="kode_komoditi" label="Kode Komoditi" key="kode_komoditi">
                <InputNumber type="number" min={1} key="kode_komoditi"/>
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}} name="nama_komoditi" label="Nama Komoditi" key="nama_komoditi" rules={[{required: true}]}>
                <Input type="text" key="nama_komoditi"/>
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}} name="jenis" label="Jenis" key="jenis" rules={[{required: true}]}>
                <_Select option={listData} val="jenis" caption="jenis" />
            </Form.Item>
            <Form.Item name="deskripsi" label="Deskripsi" key="deskripsi" >
                <TextArea showCount maxLength={100} style={{ height: 120 }} allowClear/>
            </Form.Item>
        </Form>
    )
}
