import React, { useEffect, useState } from "react";
import _MainLayouts from "../../../layouts/_MainLayouts";
import {
  _Button,
  _Checkbox,
  _Date,
  _Input,
  _Label,
  _Select,
  _Switch,
  _TitleBar,
} from "../../../services/Forms/Forms";
import {
  Table,
  Form,
  Pagination,
  Row,
  Col,
  Button,
  Popconfirm,
  message,
  Input,
  InputNumber,
  Select
} from "antd";
import { PlusOutlined, DeleteFilled, EditFilled, SaveFilled } from "@ant-design/icons";
import { _Col, _Row } from "../../../services/Forms/LayoutBootstrap";
import _Api from "../../../services/Api/_Api";
import { _Toastr } from "../../../services/Toastr/Notify/_Toastr";
import _AutocompleteRev from "../../../services/Forms/_AutocompleteRev";
import ModalTambahKomoditas from "./ModalTambahKomoditas";

const {TextArea} = Input
const { Option } = Select;

function Komoditas() {

  const [form] = Form.useForm();
  const [input, setinput] = useState(false)
  const [paginate, setpaginate] = useState({
      total : 10,
      from : 1
  })
  const [page, setPage] = useState({
    size : 10,
    cp : 1,
})
  const [dataSurvey, setdataSurvey] = useState([])
  const [listData, setListData] = useState([
    {id: 1, jenis: "Bahan Pokok"},
    {id: 2, jenis: "Bahan Penting"},
  ])
  const [selected, setselected] = useState("")
  const [loadingDel, setloadingDel] = useState(false)
  const [visible, setVisible] = useState(false);

  const [editingKey, setEditingKey] = useState('');
  const isEditing = (record) => record.id === editingKey;



  const [formData] = Form.useForm()
  const showTambahModal = ()=>{
    setVisible(true)
  }

  const onCancel = () => {
    setVisible(false) 
  }

  const edit = (record) => {
    form.setFieldsValue({
      kode_komoditi: null,
      nama_komoditi:'',
      deskripsi: null,
      ...record,
    });
    setEditingKey(record.id);
  };

  const cancelEdit = () => {
    setEditingKey('');
  };

  const saveEdit = async (record) => {
    try {
      const row = await form.validateFields();
      const dataSave = {...row}
      console.log(dataSave)
      putEdit(record.id, dataSave)
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const EditableCell = ({
    editing,
    dataIndex,
    title,
    record,
    index,
    children,
    ...restProps
  }) => {
    const itemProject = () => {
      switch (dataIndex) {
        case "kode_komoditi":
          return(<Form.Item name="kode_komoditi" style={{margin: 0,}}><InputNumber min={1} /></Form.Item>)
          break;
        case "nama_komoditi":
          return(<Form.Item
              name="nama_komoditi"
              style={{margin: 0,}}
              rules={[{
                  required: true,
                  message: `Please Input ${title}!`,
                },
              ]}
            >
              <Input />
          </Form.Item>)
          break;
        case "jenis":
          return(<Form.Item
              name="jenis"
              style={{margin: 0,}}
              rules={[{
                  required: true,
                  message: `Please Input ${title}!`,
                },
              ]}
          >
            <Select>
              <Option value="Bahan Pokok">Bahan Pokok</Option>
              <Option value="Bahan Penting">Bahan Penting</Option>
            </Select>
          </Form.Item>)
          break;
        default:
          return(<Form.Item name="deskripsi" style={{margin: 0,}}>
              <TextArea allowClear/>
          </Form.Item>)
          break;
      }
    }
    return (
      <td {...restProps}>
        {editing ? (
          <Form
          form={form} 
          >
            {
              (dataIndex=="kode_komoditi" && <Form.Item name="kode_komoditi" style={{margin: 0,}}><InputNumber type="number" min={1} /></Form.Item>)
              || (dataIndex=="nama_komoditi" && 
              <Form.Item
              name="nama_komoditi"
              style={{margin: 0,}}
                rules={[{
                    required: true,
                    message: `Please Input ${title}!`,
                  },
                ]}
                >
                <Input />
              </Form.Item>)
              || (dataIndex=="jenis" && 
              <Form.Item
              name="jenis"
              style={{margin: 0,}}
              rules={[{
                required: true,
                message: `Please Input ${title}!`,
              },
              ]}
              >
                <Select>
                  <Option value="Bahan Pokok">Bahan Pokok</Option>
                  <Option value="Bahan Penting">Bahan Penting</Option>
                </Select>
              </Form.Item>)
              || (dataIndex=="deskripsi" && <Form.Item name="deskripsi" style={{margin: 0,}}><TextArea allowClear/></Form.Item>)
            
            }
          </Form>
        ) : (
          children
        )}
      </td>
    );
  };

  const putEdit = (id, values) => {
    _Api.put(`komoditi/${id}`,values)
    .then(() => {
        message.success('Update Berhasil', 3)
        loadData(page.cp, page.size)
        setEditingKey('');
    })
    .catch(info => {
        message.error('Update Gagal', 3)
    })
  }

  const columns = [
    {
      title: "No",
      width: 50,
      align: "center",
      render: (text, row, index) => <> {paginate.from + index } </>,
    },
    {
      title: "Kode Komoditi",
      width: 100,
      editable: true,
      dataIndex: 'kode_komoditi',
      sorter: (a, b) => a.kode_komoditi.length - b.kode_komoditi.length,
      render: (_, rc) => (
      <div> {rc.kode_komoditi}</div>
      ),
    },
    {
        title: "Nama Komoditi",
        width: 250,
        editable: true,
        dataIndex: 'nama_komoditi',
        sorter: (a, b) => a.nama_komoditi.length - b.nama_komoditi.length,
        render: (_, rc) => (
          <div> {rc.nama_komoditi}</div>
        ),
    },
    {
      title: "Jenis",
      width: 200,
      editable: true,
      dataIndex: 'jenis',
      sorter: (a, b) => a.jenis.length - b.jenis.length,
      render: (_, rc) => (
        <div> {rc.jenis}</div>
      ),
    },
    {
        title: "Deskripsi",
        width: 200,
        editable: true,
        dataIndex: 'deskripsi',
        sorter: (a, b) => a.deskripsi.length - b.deskripsi.length,
        render: (_, rc) => (
          <div> {rc.deskripsi}</div>
        ),
    },
    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 150,
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <>
            <Popconfirm title="Yakin Untuk menyimpan Data?" onConfirm={()=> saveEdit(record.id)}>
              <Button style={{marginRight: "5px"}} type="primary" ><SaveFilled /></Button>
            </Popconfirm>
            <Button style={{marginRight: "5px"}}  danger type="primary" onClick={cancelEdit}>Batal</Button>
          </>
        ): (
          <>
            <Popconfirm title="Yakin Ingin Menghapus Data?" onConfirm={() => deleteData()} key="popup1">
              <Button disabled={editingKey !== ''}  type="primary" style={{marginRight: "5px"}} danger><DeleteFilled /></Button>
            </Popconfirm>
            <Button disabled={editingKey !== ''} type="primary" onClick={() => edit(record)}><EditFilled /></Button>
          </>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });


  const deleteData = () => {
    if(selected.length === 0){
        message.warning('Data Belum Dipilih');
        return
    }
    _Api.delete(`komoditi/${selected.id}`)
    .then((res)=>{
        message.success('Hapus Berhasil');
        loadData(page.cp, page.size)
    })
    .catch((info) => {
        message.error('Hapus gagal');
    })
}

  const loadData = (cp, size) => {
    setloadingDel(true)
    _Api.get(`komoditi?paginate=true&page=${cp}&per_page=${size}`).then(res => {
      setloadingDel(false)
      console.log('res.data :>> ', res.data);
      setdataSurvey(res.data.data)
      setpaginate(res.data.meta)
      // setMataKuliah(res.data)
    })
  }

  const onChangePageSize = (cp, size) => {
      console.log(cp,size)
      setPage({size : size, cp : cp})
      loadData(cp, size)
  }


  useEffect(() => {
    loadData(1,10)
  }, [])

  return (
    <_MainLayouts>

      <_TitleBar label=" DATA MASTER KOMODITAS" />
      <p style={{ marginBlock: "10px" }}></p>
      <ModalTambahKomoditas
          visible={visible}
          onCancel={() => onCancel()}
          getData={loadData}
          listData={listData}
      />
      <Row style={{padding: "5px"}}>
          <Col span={24}>
            <Button type="primary" style={{marginRight: '5px'}} onClick={()=> showTambahModal()} icon={<PlusOutlined/>}>Tambah</Button>
          </Col>
      </Row>
      <Table
        rowKey="id"
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        pagination={{ position: [], pageSize: page.size }} loading={loadingDel}
        columns={mergedColumns} dataSource={dataSurvey}
        scroll={{ x: 1, y: 1000 }}
        rowClassName={(record, index) => record == selected && 'bg-selected'}
        onRow={(record, rowIndex) => {
          return {
            onClick: event => {
              setselected(record)
            },
          };
        }}

      />
      <br />
      <Pagination style={{ width: "100%", textAlign: "center" }} 
                defaultCurrent={1} 
                total={parseInt(paginate.total)} 
                onChange={(cp, size) => onChangePageSize(cp, size)} 
                />

    </_MainLayouts>
  );
}

export default Komoditas;
