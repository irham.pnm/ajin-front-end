import { Modal, Form, message } from 'antd';
import _Api from '../../../services/Api/_Api';
import FormTambahDesa from './FormTambahDesa';


const ModalTambahDesa = ({ visible, onCancel, getData, listData }) => {
    const [form] = Form.useForm();

    const handleOk = () => {
        form
          .validateFields()
          .then((values) => {
            let dataSave = values
            console.log(dataSave)
            postData(dataSave)
          })
          .catch(() => {
                message.error('Lengkapi Data', 3)
          });
    };

    const postData = (values) => {
        _Api.post('w/desa/',values)
        .then(() => {
            message.success('Simpan Berhasil', 3)
            getData(1,10)
            handleCancel()
        })
        .catch(info => {
            message.error('Simpan Gagal', 3)
        })
    }

    const handleCancel = () => {
        form.resetFields();
        onCancel()
    }

    
    return (
      <Modal
        visible={visible}
        title="Tambah Desa"
        okText="Simpan"
        cancelText="Batal"
        onCancel={handleCancel}
        onOk={handleOk}
        closable={false}
        destroyOnClose={true}
      >
        <FormTambahDesa form={form} listData={listData}/>
      </Modal>
    );
  };

  export default ModalTambahDesa;