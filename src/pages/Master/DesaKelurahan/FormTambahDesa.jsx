import { Form, Input } from 'antd';
import { _Select } from '../../../services/Forms/Forms';

const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

export default function FormTambahDesa({form, listData}) {
    return (
        <Form {...layout} 
        form={form} 
        name="control-hooks"
        >
            <Form.Item name="nama_desa" label="Nama Desa/Kelurahan" key="nama_desa" rules={[{required: true}]}>
                <Input type="text" key="nama_desa"/>
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}}name="kecamatan_id" label="Kecamatan" key="kecamatan_id" rules={[{required: true}]}>
                <_Select option={listData} val="id" caption="nama_kecamatan" />
            </Form.Item>
        </Form>
    )
}
