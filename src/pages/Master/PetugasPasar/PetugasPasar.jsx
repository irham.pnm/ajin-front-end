import React, { useEffect, useState } from "react";
import { _Button, _Checkbox, _Date, _Input, _Label, _Select, _Switch, _TitleBar, } from "../../../services/Forms/Forms";
import { Table, Radio, Divider, Input, Button, Form, Avatar, Drawer, Space, DatePicker, Spin, Popconfirm, Tooltip, Badge, Tag, Progress, Image, Rate, Checkbox, Modal, Pagination, Select, } from "antd";
import moment from "moment";
import { fitrah, formatNumber, globalText, _Role } from "../../../services/Text/GlobalText";
// import { _Col, _Row } from "../../services/Forms/LayoutBootstrap";
import { CheckOutlined, ClusterOutlined, DeleteOutlined, DownloadOutlined, EditOutlined, FileDoneOutlined, MinusCircleTwoTone, PlusCircleTwoTone, SecurityScanOutlined, UpCircleOutlined } from "@ant-design/icons";
// import _Api from "../../services/Api/_Api";
// import { _Toastr } from "../../services/Toastr/Notify/_Toastr";
// import _Autocomplete from "../../services/Forms/_Autocomplete";
// import _AutocompleteRev from "../../services/Forms/_AutocompleteRev";
// import { Cache } from "../../services/Cache";
import { useLocation, withRouter } from "react-router-dom";
import _MainLayouts from "../../../layouts/_MainLayouts";
import _Api from "../../../services/Api/_Api";
import { _Toastr } from "../../../services/Toastr/Notify/_Toastr";
import { _Col, _Row } from "../../../services/Forms/LayoutBootstrap";
import { Cache } from "../../../services/Cache";
// import { globalText } from "../../../services/Text/GlobalText";
// import { _Button } from "../../../services/Forms/Forms";

function PetugasPasar(pr) {

  const [input, setinput] = useState(false)
  const [paginate, setpaginate] = useState({})
  const [dataSurvey, setdataSurvey] = useState([])
  const [selected, setselected] = useState("")
  const [loadingDel, setloadingDel] = useState(false)
  const [dataItem, setdataItem] = useState(null)
  const [petugasPasar, setpetugasPasar] = useState([])
  const [currPage, setcurrPage] = useState("1")
  const location = useLocation();


  const [formData] = Form.useForm()
  const [formVerif] = Form.useForm()

  const [showInput, setshowInput] = useState(false)
  const [dataVerif, setdataVerif] = useState({})
  const [idd, setidd] = useState(null)
  const [dataPetugas, setdataPetugas] = useState([])

  var jenisBahan = pr.match.params.bahan


  //   alamat: "Jl. Sultan Salahuddin/Wadumbolo Kel. Dara Kec.Rasanae Barat"
  // email: "34543@gmail.com"
  // id: "2c46673f-7c83-483c-a0b9-2ae2deefef39"
  // jk: "L"
  // nama: "Nurcaya, SE"
  // nik: "-"
  // pasar: {id: "e312f3fa-0233-4bee-aea1-40b3643abfce", nama_pasar: "Pasar Woha",…}
  // desa: {id: "509513ca-9172-11eb-88f4-000c29e7e53b", nama_desa: "Tente",…}
  // id: "e312f3fa-0233-4bee-aea1-40b3643abfce"
  // nama_pasar: "Pasar Woha"
  // profile_image: "http://127.0.0.1:8000/assets/app/avatar/default/profile-image.png"
  // role: "petugas-pasar"
  // status: "aktif"
  // telepon: "082247495822"
  // username: "nurcaya"

  const columns = [
    {
      title: "No",
      width: 100,
      align: "center",
      render: (text, row, index) => <> {index + 1} </>,
    },
    {
      title: "NIK",
      width: 200,
      sorter: true,
      render: (_, rc) => (
        <div> {rc.nik}</div>
      ),
    },
    {
      title: "Petugas Pasar",
      width: 200,
      sorter: (a, b) => a.nama.length - b.nama.length,
      render: (_, rc) => (
        <div> {rc.nama}</div>
      ),
    },

    {
      title: "No. HP",
      width: 200,
      sorter: (a, b) => a.telepon.length - b.telepon.length,
      render: (_, rc) => (
        <div> {rc.telepon}</div>
      ),
    },
    {
      title: "Jenis Kelamin",
      width: 100,
      sorter: (a, b) => a.jk.length - b.jk.length,
      render: (_, rc) => (
        <div> {rc.jk}</div>
      ),
    },
    {
      title: "Email",
      width: 200,
      sorter: (a, b) => a.email.length - b.email.length,
      render: (_, rc) => (
        <div> {rc.email}</div>
      ),
    },
    {
      title: "Role",
      width: 200,
      sorter: (a, b) => a.role.length - b.role.length,
      render: (_, rc) => (
        <div> {rc.role}</div>
      ),
    },
    {
      title: "Status",
      width: 200,
      sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.status}</div>
      ),
    },
    {
      width: 200,
      title: "Hapus / Update",
      render: (_, rc) => (
        <div style={{ display: "flex" }}>
          <Popconfirm
            title="Hapus Petugas ?"
            onConfirm={() => hapusData(rc.id)}
            // onCancel={cancel}
            okText="Hapus"
            cancelText="Batal"
          >
            <_Button icon={<DeleteOutlined />} sm={3} loading={rc.id == idd ? true : false} block color="red" />
          </Popconfirm>
          <_Button icon={<EditOutlined />} sm={3} color="orange" onClick={() => editData(rc)} />
        </div>
      ),
    },



  ];

  const editData = (item) => {
    setdataItem(item)
    setshowInput(true)
    formData.setFieldsValue(item)
  }

  var ses = Cache.get(globalText.x_auth_resu)
  var us = {}
  if (ses) { us = JSON.parse(ses) }




  const simpanData = (val) => {
    setloadingDel(true)
    const root = dataItem ?
      _Api.put(`manajemen-pengguna/update/${dataItem.id}`, val)
      : _Api.post(`manajemen-pengguna/store`, val)
    root.then(res => {
      setshowInput(false)
      setloadingDel(false)
      setdataItem(null)
      loadData()
      _Toastr.success(res.data.message)
    })
  }

  const hapusData = (id) => {
    setidd(id)
    _Api.delete(`manajemen-pengguna/delete/${id}`).then(res => {
      setshowInput(false)
      setdataItem(null)
      loadData()
      _Toastr.success(res.data.message)
    })
  }

  const loadData = (val, pg) => {
    setloadingDel(true)
    _Api.get(`manajemen-pengguna?with=pasar.desa.kecamatan.kabupaten_kota`).then(res => {
      setloadingDel(false)
      console.log('res.data :>> ', res.data.data);
      setdataPetugas(res.data.data)
    })
  }

  const role = [
    { id: "admin", val: "Admin" },
    { id: "petugas-pasar", val: "Petugas Pasar" },
    { id: "super-admin", val: "Super Admin" },
    { id: "kepala-pasar", val: "Kepala Pasar" },
    { id: "pimpinan", val: "Pimpinan" },
  ]

  const tambahPetugas = () => {
    setdataItem(null)
    formData.resetFields()

    setshowInput(true)
  }

  useEffect(() => {
    loadData()
  }, [])

  return (
    <_MainLayouts>
      <div style={{ height: "100vh" }} className="site-drawer-render-in-current-wrapper">
        {
          showInput &&
          <Drawer
            title="Petugas Pasar"
            placement="top"
            height={1000}
            closable={false}
            // onClose={this.onClose}
            visible={true}
            getContainer={false}
            style={{ position: 'absolute' }}
          >
            <Form labelCol={{ span: 8 }} onFinish={simpanData} wrapperCol={{ span: 12 }} form={formData} >
              <_Input label="NIK" name="nik" />
              <_Input label="Nama" name="nama" />
              <_Input label="Telpon" name="telepon" />
              <_Input label="Alamat" name="alamat" multiline />
              <_Input label="Username" name="username" />
              <_Input label="Email" name="email" />
              <_Input label="Password" password name="password" />
              <_Input label="Password Lagi" password name="password_lagi" />

              <_Select label="Status" name="status" required
                option={[{ id: "aktif", val: "Aktif" }, { id: "tidak-aktif", val: "Tidak Aktif" }]} val="id" caption="val" />

              <_Select label="Role" name="role" required
                option={role} val="id" caption="val" />

              <_Select label="Jenis Kelamin" name="jk" required
                option={[{ id: "P", val: "Perampuan" }, { id: "L", val: "Laki-laki" }]} val="id" caption="val" />
              <br />
              <_Row>
                <_Col sm={3} />
                <_Button label="Simpan" icon={<CheckOutlined />} loading={loadingDel} submit sm={3} block />
                <_Button label="Batal" btnCancel color="orangered" sm={2} block onClick={() => setshowInput(false)} />
              </_Row>
            </Form>
          </Drawer>
        }



        <_TitleBar label={`Data Petugas Pasar `} />
        <p style={{ marginBlock: "10px" }}></p>
        <p>
          <_Button label="Tambah Petugas" sm={3} block onClick={tambahPetugas} />
        </p>

        <Table
          rowKey="id"
          pagination={{ pageSize: 10 }}

          // loading={loadingDel}
          columns={columns} dataSource={dataPetugas}
          scroll={{ x: 1, y: 1000 }}
          rowClassName={(record, index) => record == selected && 'bg-selected'}
          onRow={(record, rowIndex) => {
            return {
              onClick: event => {
                setselected(record)
              },
            };
          }}

        />
        <br />


      </div>


    </_MainLayouts>
  );
}

export default withRouter(PetugasPasar);
