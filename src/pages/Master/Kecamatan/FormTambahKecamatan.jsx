import { Form, Input, Select } from 'antd';
import { _Select } from '../../../services/Forms/Forms';

const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

export default function FormTambahKecamatan({form, listData}) {
    return (
        <Form {...layout} 
        form={form} 
        name="control-hooks"
        >
            <Form.Item name="nama_kecamatan" label="Nama Kecamatan" key="nama_kecamatan" rules={[{required: true}]}>
                <Input type="text" key="nama_kecamatan"/>
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}}name="kabupaten_kota_id" label="Kabupaten / Kota" key="kabupaten_kota_id" rules={[{required: true}]}>
                <_Select option={listData} val="id" caption="nama_kabupaten" />
            </Form.Item>
        </Form>
    )
}
