import { Modal, Form, message } from 'antd';
import _Api from '../../../services/Api/_Api';
import FormTambahKecamatan from './FormTambahKecamatan';


const ModalTambahKecamatan = ({ visible, onCancel, getData, listKabupaten }) => {
    const [form] = Form.useForm();

    const handleOk = () => {
        form
          .validateFields()
          .then((values) => {
            let dataSave = values
            console.log(dataSave)
            postData(dataSave)
          })
          .catch(() => {
                message.error('Lengkapi Data', 3)
          });
    };

    const postData = (values) => {
        _Api.post('w/kecamatan/',values)
        .then(() => {
            message.success('Simpan Berhasil', 3)
            getData(1,10)
            handleCancel()
        })
        .catch(info => {
            message.error('Simpan Gagal', 3)
        })
    }

    const handleCancel = () => {
        form.resetFields();
        onCancel()
    }

    
    return (
      <Modal
        visible={visible}
        title="Tambah Kecamatan"
        okText="Simpan"
        cancelText="Batal"
        onCancel={handleCancel}
        onOk={handleOk}
        closable={false}
        destroyOnClose={true}
      >
        <FormTambahKecamatan form={form} listData={listKabupaten}/>
      </Modal>
    );
  };

  export default ModalTambahKecamatan;