import React, { useEffect, useState } from "react";
import { _Button, _Checkbox, _Date, _Input, _Label, _Select, _Switch, _TitleBar, } from "../../../services/Forms/Forms";
import { Table, Radio, Divider, Input, Button, Form, Avatar, Drawer, Space, DatePicker, Spin, Popconfirm, Tooltip, Badge, Tag, Progress, Image, Rate, Checkbox, Modal, Pagination, Select, } from "antd";
import moment from "moment";
import { fitrah, formatNumber, globalText, _Role } from "../../../services/Text/GlobalText";
// import { _Col, _Row } from "../../services/Forms/LayoutBootstrap";
import { AppstoreAddOutlined, CheckOutlined, ClusterOutlined, DeleteOutlined, DownloadOutlined, EditOutlined, FileDoneOutlined, MinusCircleTwoTone, PlusCircleTwoTone, ReloadOutlined, SecurityScanOutlined, UpCircleOutlined } from "@ant-design/icons";
// import _Api from "../../services/Api/_Api";
// import { _Toastr } from "../../services/Toastr/Notify/_Toastr";
// import _Autocomplete from "../../services/Forms/_Autocomplete";
// import _AutocompleteRev from "../../services/Forms/_AutocompleteRev";
// import { Cache } from "../../services/Cache";
import { useLocation, withRouter } from "react-router-dom";
import _MainLayouts from "../../../layouts/_MainLayouts";
import _Api from "../../../services/Api/_Api";
import { _Toastr } from "../../../services/Toastr/Notify/_Toastr";
import { _Col, _Row } from "../../../services/Forms/LayoutBootstrap";
import { Cache } from "../../../services/Cache";
// import { globalText } from "../../../services/Text/GlobalText";
// import { _Button } from "../../../services/Forms/Forms";

function Varian(pr) {

  const [input, setinput] = useState(false)
  const [paginate, setpaginate] = useState({})
  const [dataSurvey, setdataSurvey] = useState([])
  const [selected, setselected] = useState("")
  const [loadingDel, setloadingDel] = useState(false)
  const [dataItem, setdataItem] = useState(null)
  const [petugasPasar, setpetugasPasar] = useState([])
  const [currPage, setcurrPage] = useState("1")
  const location = useLocation();


  const [formData] = Form.useForm()
  const [formVerif] = Form.useForm()

  const [showInput, setshowInput] = useState(false)
  const [dataVerif, setdataVerif] = useState({})
  const [idd, setidd] = useState(null)
  const [datas, setdatas] = useState([])
  const [komoditi, setkomoditi] = useState([])
  const [icon, seticon] = useState([])
  const [satuan, setsatuan] = useState([])

  var jenisBahan = pr.match.params.bahan


  //   has_het: "tidak"
  // icon: "https://ajin.disdag.ntbprov.go.id/assets/app/icons/icon daging ayam.png"
  // id: "c51cfd7b-f9ee-4f4a-a461-4bdc5adc76b5"
  // kode_varian: "17"
  // komoditi: {id: "8cb33ce3-5781-4d32-8c1b-c0681fab64f7", kode_komoditi: 6, nama_komoditi: "Daging Ayam",…}
  // deskripsi: null
  // id: "8cb33ce3-5781-4d32-8c1b-c0681fab64f7"
  // jenis: "Bahan Pokok"
  // kode_komoditi: 6
  // nama_komoditi: "Daging Ayam"
  // komoditi_id: "8cb33ce3-5781-4d32-8c1b-c0681fab64f7"
  // nama_varian: "Ayam Kampung"
  // satuan: "Kg"


  const columns = [
    {
      title: "No",
      width: 100,
      align: "center",
      render: (text, row, index) => <> {index + 1} </>,
    },

    {
      title: "Jenis Komoditi",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.komoditi && rc.komoditi.jenis}</div>
      ),
    },
    {
      title: "Nama Komoditi",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.komoditi && rc.komoditi.nama_komoditi}</div>
      ),
    },
    {
      title: "Satuan",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.satuan}</div>
      ),
    },
    {
      title: "Kode Varian",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.kode_varian}</div>
      ),
    },
    {
      title: "Varian",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.nama_varian}</div>
      ),
    },
    {
      title: "HET",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.has_het}</div>
      ),
    },

    {
      title: "Icon",
      width: 50,
      render: (_, rc) => (
        // <div></div>
        <Image src={rc.icon ? rc.icon_url : ""} alt="-" width={20} preview={false} />
        // <div> {<UpCircleOutlined style={{ fontSize: "20px", fontWeight: "bold", color: "green" }} />} </div>
      ),
    },

    {
      width: 200,
      title: "Hapus / Update",
      render: (_, rc) => (
        <div style={{ display: "flex" }}>
          <Popconfirm
            title="Hapus Petugas ?"
            onConfirm={() => hapusData(rc.id)}
            // onCancel={cancel}
            okText="Hapus"
            cancelText="Batal"
          >
            <_Button icon={<DeleteOutlined />} sm={3} loading={rc.id == idd ? true : false} block color="red" />
          </Popconfirm>
          <_Button icon={<EditOutlined />} sm={3} color="orange" onClick={() => editData(rc)} />
        </div>
      ),
    },



  ];

  const editData = (item) => {
    setdataItem(item)
    setshowInput(true)
    formData.setFieldsValue(item)
  }

  var ses = Cache.get(globalText.x_auth_resu)
  var us = {}
  if (ses) { us = JSON.parse(ses) }




  const simpanData = (val) => {
    setloadingDel(true)
    const root = dataItem ?
      _Api.put(`varian/${dataItem.id}`, val)
      : _Api.post(`varian`, val)
    root.then(res => {
      setshowInput(false)
      setloadingDel(false)
      setdataItem(null)
      loadData()
      _Toastr.success(res.data.message)
    })
  }

  const hapusData = (id) => {
    setidd(id)
    _Api.delete(`varian/${id}`).then(res => {
      setshowInput(false)
      setdataItem(null)
      loadData()
      _Toastr.success(res.data.message)
    })
  }

  const loadData = (val, pg) => {
    setloadingDel(true)
    _Api.get(`varian?with=komoditi`).then(res => {
      setloadingDel(false)
      setdatas(res.data.data)
    })
  }

  const loadCombo = (val, pg) => {
    setloadingDel(true)
    _Api.get(`komoditi`).then(res => {
      setkomoditi(res.data.data)
    })
    _Api.get(`satuan`).then(res => {
      setsatuan(res.data.data)
    })
    _Api.get(`satuan`).then(res => {
      seticon(res.data.data)
    })
  }


  const iniIcon = [
    { val: "icon cabai.png", caption: "Cabai" },
    { val: "icon daging ayam.png", caption: "Daging Ayam" },
    { val: "icon ikan.png", caption: "Ikan" },
    { val: "icon jagung.png", caption: "Jagung" },
    { val: "icon kacang.png", caption: "Kacang" },
    { val: "icon karung.png", caption: "Karung" },
    { val: "icon kedelai.png", caption: "Kedelai" },
    { val: "icon ketela.png", caption: "Ketela" },
    { val: "icon mie.png", caption: "Mie" },
    { val: "icon minyak.png", caption: "Minyak" },
    { val: "icon telur.png", caption: "Telur" },
  ]

  const hasHet = [
    { id: "ya", val: "Ya" },
    { id: "tidak", val: "Tidak" },

  ]

  const tambahPetugas = () => {
    setdataItem(null)
    formData.resetFields()

    setshowInput(true)
  }

  useEffect(() => {
    loadData()
    loadCombo()
  }, [])

  return (
    <_MainLayouts>
      <div style={{ height: "100vh" }} className="site-drawer-render-in-current-wrapper">
        {
          showInput &&
          <Drawer
            title="Proses Data Varian"
            placement="left"
            width={1000}
            height={1000}
            closable={false}
            // onClose={this.onClose}
            visible={true}
            getContainer={false}
            style={{ position: 'absolute' }}
          >
            <Form layout="vertical" form={formData} onFinish={simpanData} >

              <_Select label="Komoditi" name="komoditi_id" required
                option={komoditi} val="id" caption="nama_komoditi" />

              <_Select label="Satuan" name="satuan_id" required
                option={satuan} val="id" caption="nama_satuan" />

              <_Input label="Kode Varian" name="kode_varian" />
              <_Input label="Nama Varian" name="nama_varian" />

              <_Select label="Icon" name="icon" required
                option={iniIcon} val="val" caption="caption" />

              <_Select label="HET" name="has_het" required
                option={hasHet} val="id" caption="val" />


              <br />
              <_Row>
                <_Col sm={3} />
                <_Button label="Simpan" icon={<CheckOutlined />} loading={loadingDel} submit sm={3} block />
                <_Button label="Batal" btnCancel color="orangered" sm={2} block onClick={() => setshowInput(false)} />
              </_Row>
            </Form>
          </Drawer>
        }



        <_TitleBar label={`Data Varian`} />
        <p style={{ marginBlock: "10px" }}></p>
        <_Row>

          <_Button label="Tambah Data" sm={3} color="orange" block onClick={tambahPetugas} block icon={<AppstoreAddOutlined />} />
          <_Button label="Referesh" sm={2} onClick={loadData} block icon={<ReloadOutlined />} />
        </_Row>

        <p style={{ marginBlock: "10px" }}></p>

        <Table
          rowKey="id"
          pagination={{ pageSize: 10 }}

          // loading={loadingDel}
          columns={columns} dataSource={datas}
          scroll={{ x: 1, y: 1000 }}
          rowClassName={(record, index) => record == selected && 'bg-selected'}
          onRow={(record, rowIndex) => {
            return {
              onClick: event => {
                setselected(record)
              },
            };
          }}

        />
        <br />


      </div>


    </_MainLayouts>
  );
}

export default withRouter(Varian);
