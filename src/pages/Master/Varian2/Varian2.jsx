import React, { useEffect, useState } from "react";
import _MainLayouts from "../../../layouts/_MainLayouts";
import {
  _Button,
  _Checkbox,
  _Date,
  _Input,
  _Label,
  _Select,
  _Switch,
  _TitleBar,
} from "../../../services/Forms/Forms";
import {
  Table,
  Form,
  Pagination,
  Row,
  Col,
  Button,
  Popconfirm,
  message,
  Input,
  InputNumber,
  Select,
} from "antd";
import { PlusOutlined, DeleteFilled, EditFilled, SaveFilled } from "@ant-design/icons";
import { _Col, _Row } from "../../../services/Forms/LayoutBootstrap";
import _Api from "../../../services/Api/_Api";
import { _Toastr } from "../../../services/Toastr/Notify/_Toastr";
import _AutocompleteRev from "../../../services/Forms/_AutocompleteRev";
import ModalTambahVarian from "./ModalTambahVarian";

const {TextArea} = Input
const { Option } = Select;

function Varian2() {
  const listIcon = [
    {id: 1, value: "icon bawang.png" , caption: "Bawang" },
    {id: 2, value: "icon cabai.png" , caption: "Cabai" },
    {id: 3, value: "icon daging.png" , caption: "Daging" },
    {id: 4, value: "icon daging ayam.png" , caption: "Daging Ayam" },
    {id: 5, value: "icon ikan.png" , caption: "Ikan" },
    {id: 6, value: "icon jagung.png" , caption: "Jagung" },
    {id: 7, value: "icon kacang.png" , caption: "Kacang" },
    {id: 8, value: "icon karung.png" , caption: "Karung" },
    {id: 9, value: "icon kedelai.png" , caption: "Kedelai" },
    {id: 10, value: "icon ketela.png" , caption: "Ketela" },
    {id: 11, value: "icon mie.png" , caption: "Mie" },
    {id: 12, value: "icon minyak.png" , caption: "Minyak" },
    {id: 13, value: "icon susu.png" , caption: "Susu" },
    {id: 14, value: "icon telur.png" , caption: "Telur" },
  ]

  const listHet = [
    {id: 1, value:"ya", caption: "Ya"},
    {id: 1, value:"tidak", caption: "Tidak"},
  ]

  const [form] = Form.useForm();
  const [input, setinput] = useState(false)
  const [paginate, setpaginate] = useState({
      total : 10,
      from : 1
  })
  const [page, setPage] = useState({
    size : 10,
    cp : 1,
})
  const [dataSurvey, setdataSurvey] = useState([])
  const [listData, setListData] = useState({
    icon : listIcon,
    komoditi : [],
    satuan: [],
    het: listHet

  })
  const [selected, setselected] = useState("")
  const [loadingDel, setloadingDel] = useState(false)
  const [visible, setVisible] = useState(false);

  const iconOption = listIcon.map((item)=> {
    <Option value={item.value} key={item.value}>{item.caption}</Option>
  })

  const [editingKey, setEditingKey] = useState('');
  const isEditing = (record) => record.id === editingKey;

  



  const [formData] = Form.useForm()
  const showTambahModal = ()=>{
    setVisible(true)
  }

  const onCancel = () => {
    setVisible(false) 
  }

  const edit = (record) => {
    const satuanEdit = listData.satuan.find(item => item.nama_satuan === record.satuan)
    const slugIcon = `${record.icon.split('/').pop()}`
    console.log(slugIcon)
    form.setFieldsValue({
      satuan_id: satuanEdit.id,
      komoditi_id: null,
      nama_varian: "",
      kode_varian: "",
      has_het: null,
      icon: null,
      ...record,
      icon: slugIcon,
    });
    setEditingKey(record.id);
  };

  const cancelEdit = () => {
    setEditingKey('');
  };

  const saveEdit = async (record) => {
    try {
      const row = await form.validateFields();
      const dataSave = {...row}
      console.log(dataSave)
      putEdit(record.id, dataSave)
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const EditableCell = ({
    editing,
    dataIndex,
    title,
    record,
    index,
    children,
    ...restProps
  }) => {
    return (
      <td {...restProps}>
        {editing ? (
          <Form
          form={form} 
          >
            {
              (dataIndex=="kode_varian" && <Form.Item name="kode_varian" style={{margin: 0,}}><InputNumber type="number" min={1} /></Form.Item>)
              || 
              (dataIndex=="nama_varian" && 
              <Form.Item
              name="nama_varian"
              style={{margin: 0,}}
                rules={[{
                    required: true,
                    message: `Please Input ${title}!`,
                  },
                ]}
                >
                <Input />
              </Form.Item>)
              || 
              (dataIndex=="has_het" && 
                <_Select option={listData.het} name="has_het" val="value" caption="caption" required={true}/>)
              || 
              (dataIndex=="nama_komoditi" && 
                <_Select option={listData.komoditi} name="komoditi_id" val="id" caption="nama_komoditi" required={true}/>)
              || 
              (dataIndex=="satuan" && 
                <_Select option={listData.satuan} name="satuan_id" val="id" caption="nama_satuan" required={true}/>)
              || 
              (dataIndex=="icontable" && 
                <_Select option={listData.icon} name="icon" val="value" caption="caption" required={true}/>)
            }
          </Form>
        ) : (
          children
        )}
      </td>
    );
  };

  const putEdit = (id, values) => {
    _Api.put(`varian/${id}`,values)
    .then(() => {
        message.success('Update Berhasil', 3)
        loadData(page.cp, page.size)
        setEditingKey('');
    })
    .catch(info => {
        message.error('Update Gagal', 3)
    })
  }

  const columns = [
    {
      title: "No",
      width: 50,
      align: "center",
      render: (text, row, index) => <> {paginate.from + index } </>,
    },
    {
      title: "Kode Varian",
      width: 100,
      editable: true,
      dataIndex: 'kode_varian',
      sorter: (a, b) => a.kode_varian - b.kode_varian,
      render: (_, rc) => (
      <div> {rc.kode_varian}</div>
      ),
    },
    {
      title: "Nama Varian",
      width: 250,
      editable: true,
      dataIndex: 'nama_varian',
      sorter: (a, b) => a.nama_varian.length - b.nama_varian.length,
      render: (_, rc) => (
        <div> {rc.nama_varian}</div>
      ),
    },
    {
      title: "Komoditi",
      width: 200,
      editable: true,
      dataIndex: 'nama_komoditi',
      sorter: (a, b) => a.komoditi.nama_komoditi.length - b.komoditi.nama_komoditi.length,
      render: (_, rc) => (
        <div> {rc.komoditi?.nama_komoditi?rc.komoditi.nama_komoditi:null}</div>
      ),
    },
    {
      title: "Ada HET",
      width: 100,
      editable: true,
      dataIndex: 'has_het',
      sorter: (a, b) => a.has_het.length - b.has_het.length,
      render: (_, rc) => (
        <div> {rc.has_het}</div>
      ),
    },
    {
        title: "Satuan",
        width: 100,
        editable: true,
        dataIndex: 'satuan',
        sorter: (a, b) => a.satuan.length - b.satuan.length,
        render: (_, rc) => (
          <div> {rc.satuan}</div>
        ),
    },
    {
        title: "Icon",
        width: 120,
        editable: true,
        dataIndex: 'icontable',
        render: (_, rc) => (
          <div align="center"><img width="25px" src={rc.icon}></img></div>
        ),
    },
    {
      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 150,
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <>
            <Popconfirm title="Yakin Untuk menyimpan Data?" onConfirm={()=> saveEdit(record)}>
              <Button style={{marginRight: "5px"}} ><SaveFilled /></Button>
            </Popconfirm>
            <Button style={{marginRight: "5px"}} onClick={cancelEdit}>Cancel</Button>
          </>
        ): (
          <>
            <Popconfirm title="Yakin Ingin Menghapus Data?" onConfirm={deleteData} key="popup1">
              <Button disabled={editingKey !== ''} style={{marginRight: "5px"}} danger><DeleteFilled /></Button>
            </Popconfirm>
            <Button disabled={editingKey !== ''} onClick={() => edit(record)}><EditFilled /></Button>
          </>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });


  const deleteData = () => {
    if(selected.length === 0){
        message.warning('Data Belum Dipilih');
        return
    }
    _Api.delete(`varian/${selected.id}`)
    .then((res)=>{
        message.success('Hapus Berhasil');
        loadData(page.cp, page.size)
    })
    .catch((info) => {
        message.error('Hapus gagal');
    })
}

  const loadData = (cp, size) => {
    setloadingDel(true)
    _Api.get(`varian?with=komoditi&paginate=true&page=${cp}&per_page=${size}`).then(res => {
      setloadingDel(false)
      console.log('res.data :>> ', res.data);
      setdataSurvey(res.data.data)
      setpaginate(res.data.meta)
      // setMataKuliah(res.data)
    })
  }

  const loadCombo = () => {
    setloadingDel(true)
    _Api.get("komoditi").then(res => {
      setListData(res.data.data)
      const komoditiCombi = res.data.data
      _Api.get("satuan").then(res => {
        setListData({...listData, komoditi: komoditiCombi, satuan:res.data.data})
        setloadingDel(false)
      })
    })

  }

  const onChangePageSize = (cp, size) => {
      console.log(cp,size)
      setPage({size : size, cp : cp})
      loadData(cp, size)
  }


  useEffect(() => {
    loadData(1,10)
    loadCombo()
  }, [])

  return (
    <_MainLayouts>

      <_TitleBar label=" DATA MASTER VARIAN" />
      <p style={{ marginBlock: "10px" }}></p>
      <ModalTambahVarian
          visible={visible}
          onCancel={() => onCancel()}
          getData={loadData}
          listData={listData}
      />
      <Row style={{padding: "5px"}}>
          <Col span={24}>
            <Button type="primary" style={{marginRight: '5px'}} onClick={()=> showTambahModal()} icon={<PlusOutlined/>}>Tambah</Button>
          </Col>
      </Row>
      <Table
        rowKey="id"
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        pagination={{ position: [], pageSize: page.size }} loading={loadingDel}
        columns={mergedColumns} dataSource={dataSurvey}
        scroll={{ x: 1, y: 1000 }}
        rowClassName={(record, index) => record == selected && 'bg-selected'}
        onRow={(record, rowIndex) => {
          return {
            onClick: event => {
              setselected(record)
            },
          };
        }}

      />
      <br />
      <Pagination style={{ width: "100%", textAlign: "center" }} 
                defaultCurrent={1} 
                total={parseInt(paginate.total)} 
                onChange={(cp, size) => onChangePageSize(cp, size)} 
                />

    </_MainLayouts>
  );
}

export default Varian2;
