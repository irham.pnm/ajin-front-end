import { Form, Input, InputNumber } from 'antd';
import { _Select } from '../../../services/Forms/Forms';

const {TextArea} = Input
const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

export default function FormTambahVarian({form, listData}) {
    return (
        <Form {...layout} 
        form={form} 
        name="control-hooks"
        initialValues={{kode_varian: null}}
        >
            <Form.Item name="kode_varian" label="Kode Varian" key="kode_varian">
                <InputNumber type="number" min={1} key="kode_varian"/>
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}} name="nama_varian" label="Nama Varian" key="nama_varian" rules={[{required: true}]}>
                <Input type="text" key="nama_varian"/>
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}} name="satuan_id" label="Satuan" key="satuan_id" rules={[{required: true}]}>
                <_Select option={listData.satuan} val="id" caption="nama_satuan" />
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}} name="has_het" label="HET" key="has_het" rules={[{required: true}]}>
                <_Select option={listData.het} val="value" caption="caption" />
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}} name="komoditi_id" label="Komoditi" key="komoditi_id" rules={[{required: true}]}>
                <_Select option={listData.komoditi} val="id" caption="nama_komoditi" />
            </Form.Item>
            <Form.Item style={{marginTop: '10px'}} name="icon" label="Icon" key="icon" rules={[{required: true}]}>
                <_Select option={listData.icon} val="value" caption="caption" />
            </Form.Item>
        </Form>
    )
}
