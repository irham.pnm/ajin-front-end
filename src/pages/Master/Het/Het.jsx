import React, { useEffect, useState } from "react";
import { _Button, _Checkbox, _Date, _Input, _Label, _Select, _Switch, _TitleBar, } from "../../../services/Forms/Forms";
import { Table, Radio, Divider, Input, Button, Form, Avatar, Drawer, Space, DatePicker, Spin, Popconfirm, Tooltip, Badge, Tag, Progress, Image, Rate, Checkbox, Modal, Pagination, Select, } from "antd";
import moment from "moment";
import { fitrah, formatNumber, globalText, _Role } from "../../../services/Text/GlobalText";
// import { _Col, _Row } from "../../services/Forms/LayoutBootstrap";
import { AppstoreAddOutlined, CheckOutlined, ClusterOutlined, DeleteOutlined, DownloadOutlined, EditOutlined, FileDoneOutlined, MinusCircleTwoTone, PlusCircleTwoTone, ReloadOutlined, SecurityScanOutlined, UpCircleOutlined } from "@ant-design/icons";
// import _Api from "../../services/Api/_Api";
// import { _Toastr } from "../../services/Toastr/Notify/_Toastr";
// import _Autocomplete from "../../services/Forms/_Autocomplete";
// import _AutocompleteRev from "../../services/Forms/_AutocompleteRev";
// import { Cache } from "../../services/Cache";
import { useLocation, withRouter } from "react-router-dom";
import _MainLayouts from "../../../layouts/_MainLayouts";
import _Api from "../../../services/Api/_Api";
import { _Toastr } from "../../../services/Toastr/Notify/_Toastr";
import { _Col, _Row } from "../../../services/Forms/LayoutBootstrap";
import { Cache } from "../../../services/Cache";
// import { globalText } from "../../../services/Text/GlobalText";
// import { _Button } from "../../../services/Forms/Forms";

function Het(pr) {

  const [input, setinput] = useState(false)
  const [paginate, setpaginate] = useState({})
  const [dataSurvey, setdataSurvey] = useState([])
  const [selected, setselected] = useState("")
  const [loadingDel, setloadingDel] = useState(false)
  const [dataItem, setdataItem] = useState(null)
  const [petugasPasar, setpetugasPasar] = useState([])
  const [currPage, setcurrPage] = useState("1")
  const location = useLocation();


  const [formData] = Form.useForm()
  const [formVerif] = Form.useForm()

  const [showInput, setshowInput] = useState(false)
  const [dataVerif, setdataVerif] = useState({})
  const [idd, setidd] = useState(null)
  const [datas, setdatas] = useState([])
  const [varian, setvarian] = useState([])
  const [icon, seticon] = useState([])
  const [satuan, setsatuan] = useState([])

  var jenisBahan = pr.match.params.bahan

 
  const columns = [
    {
      title: "No",
      width: 100,
      align: "center",
      render: (text, row, index) => <> {index + 1} </>,
    },

    {
      title: "Varian",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.varian && rc.varian.nama_varian}</div>
      ),
    },

    {
      title: "Het",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.het && rc.het}</div>
      ),
    },
    {
      title: "Peraturan Pemerintah",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.permen && rc.permen}</div>
      ),
    },
    {
      title: "Status",
      width: 200,
      sorter: true,
      // sorter: (a, b) => a.status.length - b.status.length,
      render: (_, rc) => (
        <div> {rc.status && rc.status}</div>
      ),
    },
   
     
    

    {
      width: 200,
      title: "Hapus / Update",
      render: (_, rc) => (
        <div style={{ display: "flex" }}>
          <Popconfirm
            title="Hapus Petugas ?"
            onConfirm={() => hapusData(rc.id)}
            // onCancel={cancel}
            okText="Hapus"
            cancelText="Batal"
          >
            <_Button icon={<DeleteOutlined />} sm={3} loading={rc.id == idd ? true : false} block color="red" />
          </Popconfirm>
          <_Button icon={<EditOutlined />} sm={3} color="orange" onClick={() => editData(rc)} />
        </div>
      ),
    },



  ];

  const editData = (item) => {
    setdataItem(item)
    setshowInput(true)
    formData.setFieldsValue({...item, het : item.het.toString()})
  }

  var ses = Cache.get(globalText.x_auth_resu)
  var us = {}
  if (ses) { us = JSON.parse(ses) }




  const simpanData = (val) => {
    setloadingDel(true)
    const root = dataItem ?
      _Api.put(`het/${dataItem.id}`, val)
      : _Api.post(`het`, val)
    root.then(res => {
      setshowInput(false)
      setloadingDel(false)
      setdataItem(null)
      loadData()
      _Toastr.success(res.data.message)
    })
  }

  const hapusData = (id) => {
    setidd(id)
    _Api.delete(`het/${id}`).then(res => {
      setshowInput(false)
      setdataItem(null)
      loadData()
      _Toastr.success(res.data.message)
    })
  }

  const loadData = (val, pg) => {
    setloadingDel(true)
    _Api.get(`het?with=varian`).then(res => {
      setloadingDel(false)
      setdatas(res.data.data)
    })
  }

  const loadCombo = (val, pg) => {
    setloadingDel(true)
    _Api.get(`varian`).then(res => {
      setvarian(res.data.data)
    })
    _Api.get(`satuan`).then(res => {
      setsatuan(res.data.data)
    })
    _Api.get(`satuan`).then(res => {
      seticon(res.data.data)
    })
  }


 

  const hasStatus = [
    { id: "aktif", val: "Aktif" },
    { id: "tidak-aktif", val: "Tidak Aktif" },

  ]

  const tambahPetugas = () => {
    setdataItem(null)
    formData.resetFields()

    setshowInput(true)
  }

  useEffect(() => {
    loadData()
    loadCombo()
  }, [])

  return (
    <_MainLayouts>
      <div style={{ height: "100vh" }} className="site-drawer-render-in-current-wrapper">

        <Drawer
          title="Proses Data Het"
          placement="left"
          headerStyle={{background :"orange"}}
          width={1000}
          height={1000}
          closable={true}
          // onClose={this.onClose}
          visible={showInput}
          getContainer={false}
          style={{ position: 'absolute' }}
        >
          {
            showInput &&
            <Form layout="vertical" form={formData} onFinish={simpanData} >

              <_Select label="Varian" name="varian_id" required
                option={varian} val="id" caption="nama_varian" />

              <_Input label="Het" required  name="het" />
              <_Input label="Peraturan Menteri" required multiline name="permen" />
             

              <_Select label="Status" name="status" required
                option={hasStatus} val="id" caption="val" />


{/* "varian_id" : "4aa4217f-3b50-46c0-bfa1-4a3ae2284894",
	"het" : 10000,
	"permen" : "Permen no. 10 tahun 2019"
   */}
              <br />
              <_Row>
                <_Col sm={3} />
                <_Button label="Simpan" icon={<CheckOutlined />} loading={loadingDel} submit sm={3} block />
                <_Button label="Batal" btnCancel color="orangered" sm={2} block onClick={() => setshowInput(false)} />
              </_Row>
            </Form>
          }
        </Drawer>



        <_TitleBar label={`Master Het`} />
        <p style={{ marginBlock: "10px" }}></p>
        <_Row>

          <_Button label="" sm={1} onClick={loadData} block icon={<ReloadOutlined />} />
          <_Button label="Het Baru" sm={3}  block onClick={tambahPetugas} block icon={<AppstoreAddOutlined />} />
        </_Row>

        <p style={{ marginBlock: "10px" }}></p>

        <Table
          rowKey="id"
          pagination={{ pageSize: 10 }}

          // loading={loadingDel}
          columns={columns} dataSource={datas}
          scroll={{ x: 1, y: 1000 }}
          rowClassName={(record, index) => record == selected && 'bg-selected'}
          onRow={(record, rowIndex) => {
            return {
              onClick: event => {
                setselected(record)
              },
            };
          }}

        />
        <br />


      </div>


    </_MainLayouts>
  );
}

export default withRouter(Het);
