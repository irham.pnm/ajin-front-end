import { AreaChartOutlined, ArrowDownOutlined, ArrowRightOutlined, ArrowUpOutlined, DingdingOutlined, LoginOutlined, MailOutlined, PhoneOutlined, PushpinOutlined } from '@ant-design/icons';
import { Image } from 'antd'
import { Table, Tag, Space } from 'antd';
import React, { useEffect } from 'react'
import { useState } from 'react';
import { Link } from 'react-router-dom';
import _Nav from '../layouts/_Nav'
import _Api from '../services/Api/_Api';
import { _Date, _RadioGroup, _Select } from '../services/Forms/Forms'
import { _Col, _Row } from '../services/Forms/LayoutBootstrap'
import { formatNumber } from '../services/Text/GlobalText';
import logo from "./../assets/img/icons/logo1.png"
import up from "./../assets/img/icons/up.png"
import down from "./../assets/img/icons/down.png"
import moment from 'moment';

function Dashboard() {

  const [dataBahanPokok, setDataBahanPokok] = useState([])
  const [dataBahanPenting, setdataBahanPenting] = useState([])
  const [loading, setloading] = useState(false)
  const [lokasiPasar, setLokasiPasar] = useState([])
  const [idd_pasar, setidd_pasar] = useState("cd43615e-c157-40b7-a113-b3065fbd3e48")
  const [tanggal, settanggal] = useState(moment().format('YYYY-MM-DD'));




  const columns = [
    {
      title: 'Komoditi',
      dataIndex: 'nama_komoditi',
      key: 'Komoditi',

    },
    {
      title: 'Varian',
      dataIndex: 'nama_varian',
      key: 'Varian',
    },
    {
      title: 'Sat.',
      dataIndex: 'nama_satuan',
      key: 'Sat.',
    },

    {
      title: "Harga Kemarin",
      sorter: true,
      width: 150,
      render: (_, rc) => (
        <div style={{ textAlign: "right", marginRight: "40px", color: rc.hargasekarang < rc.hargakemarin ? "red" : "green" }}> {rc.hargakemarin && formatNumber(rc.hargakemarin)} </div>
      ),
    },
    {
      title: "Harga Hari Ini",
      sorter: true,
      width: 150,

      render: (_, rc) => (
        <div style={{ textAlign: "right", marginRight: "40px", color: rc.hargasekarang > rc.hargakemarin ? "red" : "green" }}> {rc.hargasekarang && formatNumber(rc.hargasekarang)} </div>
      ),
    },

    {
      title: "Ket",
      width: 50,
      render: (_, rc) => (
        <div>
          {
            rc.hargasekarang < rc.hargakemarin ?
              <div style={{ textAlign: "center", color: "green" }}> {<ArrowUpOutlined />} </div> :
              rc.hargasekarang > rc.hargakemarin ?
                <div style={{ textAlign: "center", color: "red" }}> {<ArrowDownOutlined />} </div> :
                <div style={{ textAlign: "center", color: "grey" }}> {<ArrowRightOutlined />} </div>
          }
        </div>
      ),
    },

  ];

  const loadData = (id_pasar, tgl) => {
    setloading(true)
    _Api.get(`land/show/hasil/survey?bahan=pokok&tanggal=${tgl}&id_pasar=${id_pasar}`).then(res => {
      setDataBahanPokok(res.data)
      setloading(false)
    })
    _Api.get(`land/show/hasil/survey?bahan=penting&tanggal=${tgl}&id_pasar=${id_pasar}`).then(res => {
      setdataBahanPenting(res.data)
      setloading(false)

    })
  }


  const loadCombo = () => {

    _Api.get("pasar").then(res => {
      setLokasiPasar(res.data.data)
      setloading(false)
    })
  }

  useEffect(() => {
    // loadData()
    loadCombo()
  }, [])

  const changeTanggal = (e) => {
    var tgl = moment(e).format("YYYY-MM-DD")

    settanggal(tgl)
    loadData(idd_pasar, tgl)
  }

  const changePasar = (id) => {

    setidd_pasar(id)
    loadData(id, tanggal)
  }


  return (
    <div>
      {/* <_Nav /> */}
      <_Col style={{ background: "rgb(15, 154, 83)", padding: "10px" }}>
        <_Row>
          <_Col sm={1} />
          <_Col sm={7} style={{ display: "flex" }}>
            <div style={{ float: "right", paddingTop: "3px", marginRight: "10px" }}>
              <Image height={60} src={logo} preview={false} />
            </div>
            <div>
              <p style={{ fontFamily: "arial", fontWeight: "bolder", fontSize: "20px", color: "#ebdbdb", width: "100%" }}> SI PEMANTAU HARGA </p>
              <p style={{ marginTop: "-27px" }} > Dinas perdagangan Kota Mataram </p>
            </div>

          </_Col>

          <_Col sm={3} style={{ display: "flex" }}>
            <p className='inimenu' > <a> <AreaChartOutlined /> BRANDA  </a> </p>
            <p className='inimenu' > <a> <DingdingOutlined /> PUBLIKASI </a> </p>
            <p className='inimenu' > <a> <PushpinOutlined /> TENTANG </a> </p>
            <p className='inimenu' > <Link style={{ color: "black" }} to="/login"> <a>  <LoginOutlined />  LOGIN  </a> </Link> </p>

          </_Col>
        </_Row>
      </_Col>


      <_Col style={{ marginTop: "100px", padding: "0px 10%" }}>
        <br />
        <_Row>
          <_Col sm={8}>
            <p> <b> INFO HARGA BAHAN POKOK PASAR PER PASAR </b> </p>
          </_Col>
          <_Select sm={2} name="pasar_id" defaultValue={idd_pasar} required option={lokasiPasar} onSelect={changePasar} val="id" caption="nama_pasar" />
          <_Date format={"DD/MM/YYYY"} defaultValue={moment()} onChange={changeTanggal} sm={2} />
        </_Row>
        <p><Table loading={loading} scroll={{ x: 1000 }} size='small' columns={columns} dataSource={dataBahanPokok} /></p>
        <p style={{ textAlign: "center" }}> Ket : <ArrowDownOutlined style={{ color: "red", fontSize: "17px" }} /> Harga Turun  &nbsp;
          <ArrowUpOutlined style={{ color: "green", fontSize: "17px" }} /> Harga Naik     &nbsp;
          < ArrowRightOutlined style={{ fontSize: "17px", fontWeight: "bold" }} /> Harga Stabil   </p> &nbsp;

      </_Col>
      <_Col style={{ marginTop: "100px", padding: "0px 10%" }}>
        <br />
        <Space> <b> INFO HARGA BAHAN PENTING PASAR PER PASAR </b>  </Space>
        <p><Table loading={loading} scroll={{ x: 1000 }} columns={columns} dataSource={dataBahanPenting} size='small' /></p>
      </_Col>

      <_Col style={{ background: "#4b545c", height: "70px", padding: "10px" }}>
        <_Row>
          <_Col sm={3} />
          <_Col sm={3} style={{ padding: "10px", fontSize: "16px", color: "white" }}>  <p> Dapatkan Informasi Terkait Di Sosial Media Kami! </p> </_Col>
          <_Col>  </_Col>
        </_Row>
      </_Col>
      <br />
      <_Col style={{ padding: "0px 10%" }}>
        <_Row>
          <_Col sm={4}> <p> <b> ALAMAT KANTOR </b> </p> </_Col>
          <_Col sm={4}> <p> <b>TAUTAN TERKAIT </b> </p> </_Col>
          <_Col sm={4}> <p> <b>KONTAK </b> </p> </_Col>
        </_Row>
        <_Row>
          <_Col sm={4}> <p> Jl. Langko No.61, Dasan Agung Baru, Kec. Selaparang, Kota Mataram, Nusa Tenggara Bar. 83125 </p> </_Col>
          <_Col sm={4}> <p> Pemprov NTB </p> </_Col>
          <_Col sm={4}> <p>
            <_Col >
              <p> <MailOutlined style={{ fontSize: "16px" }} /> &nbsp; disdagmataram@gmail.com </p>
              <p>  <PhoneOutlined /> &nbsp; (0370) 6172543 </p>
            </_Col>

          </p> </_Col>
        </_Row>
        <p style={{ textAlign: "center" }}><b> ©fhd</b> 2021 Copyright Dinas Perdagangan Kota Mataram</p>
      </_Col>




    </div>
  )
}

export default Dashboard
