import React, { useEffect, useState } from "react";
import _MainLayouts from "../../layouts/_MainLayouts";
import { _Button, _Checkbox, _Date, _Input, _Label, _Number, _RadioGroup, _Select, _Switch, _Text, _TitleBar, } from "../../services/Forms/Forms";
import { Table, Radio, Divider, Input, Button, Form, Avatar, Drawer, Space, DatePicker, Spin, Popconfirm, Tooltip, Badge, Tag, Progress, Image, Rate, Checkbox, Modal, Pagination, Select, } from "antd";
import moment from "moment";
import { fitrah, formatNumber, globalText, _Role } from "../../services/Text/GlobalText";
import { _Col, _Row } from "../../services/Forms/LayoutBootstrap";
import { CheckOutlined, ClusterOutlined, DeleteOutlined, DownloadOutlined, EditOutlined, FileDoneOutlined, MinusCircleTwoTone, PlusCircleTwoTone, SecurityScanOutlined, UpCircleOutlined } from "@ant-design/icons";
import _Api from "../../services/Api/_Api";
import { _Toastr } from "../../services/Toastr/Notify/_Toastr";
import _Autocomplete from "../../services/Forms/_Autocomplete";
import _AutocompleteRev from "../../services/Forms/_AutocompleteRev";
import { Cache } from "../../services/Cache";
import { withRouter } from "react-router-dom";
import { dataUser } from "../../services/Cache/Auth";

function TambahSurvey(pr) {


  const [loadingDel, setloadingDel] = useState(false)
  const [petugasPasar, setpetugasPasar] = useState([])
  const [listVarian, setlistVarian] = useState([])

  const [lokasiPasar, setLokasiPasar] = useState("")


  const [formData] = Form.useForm()


  var ses = Cache.get(globalText.x_auth_resu)
  var us = {}
  if (ses) { us = JSON.parse(ses) }



  const simpanSurvey = (val) => {
    setloadingDel(true)
    _Api.post(`survey`, {
      ...val,
      tgl_survey: moment(val.tgl_survey).format("DD/MM/YYYY"),
      pasar_id: lokasiPasar.id
    }).then(res => {
      setloadingDel(false)
      _Toastr.success(res.data.message)
      formData.resetFields()
    }).catch(err => {
    })
  }





  const loadCombo = () => {

    _Api.get(`manajemen-pengguna/show/${dataUser.id}?with=pasar`).then(res => {
      console.log(`res.data`, res.data.data.pasar)
      setLokasiPasar(res.data.data.pasar)

    })

    setloadingDel(true)
    _Api.get("user").then(res => {
      var str = `[${JSON.stringify(res.data.data)}]`;
      setpetugasPasar(JSON.parse(str))
      setloadingDel(false)
    })


    // _Api.get("pasar").then(res => {
    //   setLokasiPasar(res.data.data)
    //   setloadingDel(false)
    // })


  }
  const changeJenis = (e, f) => {
    formData.setFieldsValue({ varian_id: "" })
    _Api.get(`varian/bahan-${e.target.value}`).then(res => {
      setlistVarian(res.data.data)
      setloadingDel(false)
    })
  }

  useEffect(() => {

    formData.setFieldsValue({
      tgl_survey: moment()
    })
    // loadData()
    loadCombo()
  }, [])

  const jenisSurvey = [
    { label: 'Pokok', value: 'pokok' },
    { label: 'Penting', value: 'penting' },
  ];

  return (
    <_MainLayouts>


      <_TitleBar label=" TAMBAH SURVEY" />
      <br />
      <p style={{ marginBlock: "10px" }}></p>
      <Form layout={"vertical"} layout={"horizontal"}
        wrapperCol={{ span: 12 }} labelCol={{ span: 5 }} onFinish={simpanSurvey} form={formData}>
        <_Row style={{ marginBottom: "400px" }}>
          <_RadioGroup required label="Bahan" options={jenisSurvey} onChange={changeJenis} />
          <_Select label="Nama petugas pasar" name="petugas_pasar_id" option={petugasPasar} val="id" caption="nama" required />
          <_Input label="Lokasi Pasar" required value={lokasiPasar.nama_pasar} />

          <_Date label="Tanggal Survey" format={"DD/MM/YYYY"} name="tgl_survey" required />
          <_Select label="Varian" name="varian_id" option={listVarian} val="id" caption="nama_varian" required />
          <_Number label="Harga" format name="harga" required />
          <_Input label="Keterangan" multiline name="keterangan" />
          <_Col sm={3} />
          <_Button sm={3} block icon={<DownloadOutlined />} primary label="Simpan" submit style={{ marginTop: "24px" }} title=" " loading={loadingDel} />
          <_Button sm={2} block primary label="Reset" onClick={() => formData.resetFields()} color="red" btnCancel style={{ marginTop: "24px" }} title=" " loading={loadingDel} />
        </_Row>
      </Form>
      <br />
      <br />
      <hr />




    </_MainLayouts>
  );
}

export default TambahSurvey;
