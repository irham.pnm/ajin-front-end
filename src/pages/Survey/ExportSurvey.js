import React from 'react'
import { Button } from 'antd';
import { DownloadOutlined } from '@ant-design/icons';
import ReactExport from "react-export-excel";
import moment from 'moment';

function ExportSurvey(pr) {


    const ExcelFile = ReactExport.ExcelFile;
    const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
    const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

    // {
    //     "id": "01cfdb2c-363f-4f5d-85f9-69e0ab9b5be6",
    //     "petugas_pasar_id": "4c6fc105-9974-44af-ad52-42cac8b6025d",
    //     "pasar_id": "6e8e5dc5-9b5a-4662-bf9a-05087d50f505",
    //     "varian_id": "32a83491-ff61-47ad-880e-11526c4c0fd4",
    //     "varian_icon": "http://127.0.0.1:8000/assets/app/icons/icon bawang.png",
    //     "tgl_survey": "01/12/2021",
    //     "harga": 25000,
    //     "keterangan": null,
    //     "verifikasi": "ya",
    //     "valid": "ya",
    //     "status_icon": "",
    //     "status_het": null,
    //     "pasar": {
    //         "id": "6e8e5dc5-9b5a-4662-bf9a-05087d50f505",
    //         "nama_pasar": "Pasar Tana Mira"
    //     },
    //     "varian": {
    //         "id": "32a83491-ff61-47ad-880e-11526c4c0fd4",
    //         "komoditi_id": "0c467fd6-7a1d-4305-9363-54c7e9791d49",
    //         "kode_varian": "36",
    //         "nama_varian": "Bawang Merah",
    //         "icon": "icon bawang.png",
    //         "icon_url": "http://127.0.0.1:8000/assets/app/icons/icon bawang.png",
    //         "has_het": "tidak",
    //         "satuan": "Kg",
    //         "komoditi": {
    //             "id": "0c467fd6-7a1d-4305-9363-54c7e9791d49",
    //             "kode_komoditi": 16,
    //             "nama_komoditi": "Bawang Merah",
    //             "jenis": "Bahan Pokok",
    //             "deskripsi": null
    //         }
    //     },
    //     "petugas_pasar": {
    //         "id": "4c6fc105-9974-44af-ad52-42cac8b6025d",
    //         "nik": "-",
    //         "nama": "MUHAMMAD SYAIFUL",
    //         "telepon": "082341190170",
    //         "alamat": "RT.02 Rw.09 Lingkungan Sebubuk Kel. Kuang Kec. Taliwang",
    //         "jk": "L",
    //         "username": "syaiful",
    //         "email": "1234567@gmail.com",
    //         "status": "aktif",
    //         "role": "petugas-pasar",
    //         "profile_image": "http://127.0.0.1:8000/assets/app/avatar/default/profile-image.png"
    //     }
    // }


    return (
        <div>
            <ExcelFile element={<Button type="primary" ref={pr.reff} onClick={pr.ExportExcel} style={pr.style} icon={<DownloadOutlined />}> Export </Button>}>
                <ExcelSheet data={pr.dataSource} name="survey">
                <ExcelColumn label="Petugas Pasar" value={(col) => col.petugas_pasar.nama }/>
                <ExcelColumn label="Lokasi Pasar" value={(col) => col.pasar.nama_pasar}/>
                <ExcelColumn label="Varian" value={(col) => col.varian.nama_varian }/>
                <ExcelColumn label="Tanggal Survey" value={(col) => moment(col.tgl_survey).format('DD/MM/YYYY') }/>
                <ExcelColumn label="Harga" value={(col) => col.harga }/>
                <ExcelColumn label="isVerif" value={(col) => col.verifikasi }/>
                </ExcelSheet>

            </ExcelFile>
        </div>
    )
}

export default ExportSurvey
