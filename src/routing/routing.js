import React from 'react'
import { BrowserRouter, Router, Link, Route, Switch, useHistory, Redirect, HashRouter } from 'react-router-dom'
import AttemptAuth from '../auth/AttemptAuth';
// import _Login from '../auth/_Login';
import _MainLayouts from '../layouts/_MainLayouts';
import Dashboard from '../pages/Dashboard';
import DataDosen from '../pages/Dosen/DataDosen';

import Forms from '../pages/Forms'
import Home from '../pages/Home';
import DataMatakuliah from '../pages/MataKuliah/DataMatakuliah';
import InputMatakuliah from '../pages/MataKuliah/InputMatakuliah';
// import MataKuliah from '../pages/MataKuliah/MataKuliah';
import ProsesTopik from '../pages/MataKuliah/Topic/ProsesTopik';
import Topic from '../pages/MataKuliah/Topic/Topic';
import SurveyBahanPenting from '../pages/Survey/SurveyBahanPenting';
import SurveyBahanPokok from '../pages/Survey/SurveyBahanPokok';
import { Cache } from '../services/Cache';
import { ubahText } from '../services/Crypto';
// import InputPengaduanPasien from '../../MPP/InputPengaduanPasien'
import ProtectedRoute from '../services/Route/ProtectedRoute'
import { globalText, _Role } from '../services/Text/GlobalText';
import KabupatenKota from '../pages/Master/KabupatenKota/KabupatenKota'
import Kecamatan from '../pages/Master/Kecamatan/Kecamatan';
import ValidasiBahanPenting from '../pages/ValidasiSurvey/ValidasiBahanPenting';
import ValidasiBahanPokok from '../pages/ValidasiSurvey/ValidasiBahanPokok';
import DesaKelurahan from '../pages/Master/DesaKelurahan/DesaKelurahan';
import Pasar from '../pages/Master/Pasar/Pasar';
import Survey from '../pages/Survey/Survey';
import Validasi from '../pages/ValidasiSurvey/Validasi';
import TambahSurvey from '../pages/Survey/TambahSurvey';
import Satuan from '../pages/Master/Satuan/Satuan';
import Komoditas from '../pages/Master/Komoditas/Komoditas';
import PetugasPasar from '../pages/Master/PetugasPasar/PetugasPasar';
import Varian from '../pages/Master/Varian/Varian';
import Varian2 from '../pages/Master/Varian2/Varian2';
import Het from '../pages/Master/Het/Het';

function Routing() {
    const c404 = () => {
        return (
            <Redirect to={{ pathname: '/home' }} />
        )
    }


    var ses = Cache.get(globalText.x_auth_resu)
    var us = {}
    if (ses) {
        us = JSON.parse(ses)
        console.log(`us uussss`, us)
    }
    // var cek = JSON.parse()

    if (us.role == _Role.petugaspasar) {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={Dashboard} />
                    <Route path="/login" component={AttemptAuth} />
                    <ProtectedRoute path="/home" component={Home} />
                    <ProtectedRoute path="/Survey/:bahan" component={Survey} />
                    <ProtectedRoute path="/TambahSurvey" component={TambahSurvey} />
                    {/* <ProtectedRoute path="/SurveyBahanPenting" component={SurveyBahanPenting} />
                    <ProtectedRoute path="/SurveyBahanPokok" component={SurveyBahanPokok} /> */}
                    <Route path="*" component={() => c404()} />
                </Switch>
            </BrowserRouter>
        )
    } else
        if (us.role == _Role.superadmin) {
            return (
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={Dashboard} />
                        <Route path="/login" component={AttemptAuth} />
                        <ProtectedRoute path="/home" component={Home} />
                        {/* <Route path="/Topic" component={Topic} /> */}
                        <ProtectedRoute path="/Survey/:bahan" component={Survey} />
                        <ProtectedRoute path="/TambahSurvey" component={TambahSurvey} />
                        <ProtectedRoute path="/PetugasPasar" component={PetugasPasar} />
                        <ProtectedRoute path="/Varian" component={Varian} />
                        <ProtectedRoute path="/Varian2" component={Varian2} />
                        <ProtectedRoute path="/Het" component={Het} />

                        {/* <ProtectedRoute path="/SurveyBahanPenting" component={SurveyBahanPenting} />
                        <ProtectedRoute path="/SurveyBahanPokok" component={SurveyBahanPokok} /> */}
                        {/* <Route path="/InputMatakuliah" component={InputMatakuliah} /> */}
                        <Route path="/KabupatenKota" component={KabupatenKota} />
                        <Route path="/Kecamatan" component={Kecamatan} />
                        <Route path="/DesaKelurahan" component={DesaKelurahan} />
                        <Route path="/Pasar" component={Pasar} />
                        <Route path="/Satuan" component={Satuan} />
                        <Route path="/Komoditas" component={Komoditas} />

                        <Route path="/Validasi/:bahan" component={Validasi} />
                        <Route path="/Validasi-BahanPenting" component={ValidasiBahanPenting} />
                        <Route path="/Validasi-BahanPokok" component={ValidasiBahanPokok} />
                        <Route path="*" component={() => c404()} />
                    </Switch>
                </BrowserRouter>
            )
        } else {
            return (

                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={Dashboard} />
                        <Route path="/login" component={AttemptAuth} />
                        <ProtectedRoute path="/home" component={Home} />
                        <Route path="*" component={() => c404()} />
                    </Switch>
                </BrowserRouter>
            )
        }

}

export default Routing
